package security41.jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Security41JwtApplication {
    public static void main(String[] args) {
        SpringApplication.run(Security41JwtApplication.class, args);
    }
}



