package security41.jwt.common;

import lombok.Data;

@Data
public class Results<T> {

    private int code;

    private String msg;

    private T data;

    public static Results res(int code, String msg, Object data) {
        Results results = new Results();
        results.setCode(code);
        results.setMsg(msg);
        results.setData(data);
        return results;
    }

}
