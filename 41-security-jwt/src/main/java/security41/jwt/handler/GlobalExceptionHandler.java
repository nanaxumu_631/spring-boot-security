package security41.jwt.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;
import security41.jwt.common.Results;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(value = Exception.class)
    public Results handlerException(Exception e, HttpServletRequest request) {
        log.error(e.getMessage(), e);
        Results results = new Results();
        results.setCode(500);
        results.setMsg("UNKNOWN_ERROR");
        results.setData(e.getMessage());
        return results;
    }


    @ExceptionHandler(value = RuntimeException.class)
    public Results handlerRuntimeException(RuntimeException e, HttpServletRequest request) {
        log.error(e.getMessage(), e);
        Results results = new Results();
        results.setCode(500);
        results.setMsg("UNKNOWN_ERROR");
        results.setData(e.getMessage());
        return results;
    }


    @ExceptionHandler(NoHandlerFoundException.class)
    public Results handleNoHandlerFoundException(NoHandlerFoundException e, HttpServletRequest request) {
        log.error(e.getMessage(), e);
        Results results = new Results();
        results.setCode(404);
        results.setMsg("找不到接口");
        results.setData(e.getMessage());
        return results;
    }
}